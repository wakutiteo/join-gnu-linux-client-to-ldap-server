#!/bin/bash
#
# Join Ubuntu client to LDAP Server
# Colors and Formats
WARNINGCOLOR="\033[1;91m"
SUCCESSCOLOR="\033[1;92m"
INFOCOLOR="\e[1;34m"
NORMALCOLOR="\033[0m"
UNDERLINE="\033[7m"

# Orientative message Functions  
## Error function
error_msg() {
    echo -e "\n${WARNINGCOLOR}$1${NORMALCOLOR}"
    exit 1 # Exits from ther script when we call to his function
}

## Info function
info_msg() {
    echo -e "\n${INFOCOLOR}$1${NORMALCOLOR}"
}


#### Modify this variables ####
# LDAP server domain e.g. "ldap.domain.com" or "domain.com"
ldap_domain="mydomain.com"

# LDAP server IP address
ldap_ip="192.168.10.100"


## Only modify the variables below if want to configure ROAMING PROFILES on the client (By default they will not be configured ##
# The NFS server, must meet these requirements 
# /etc/export -> /moviles 192.168.1.0/24(rw,sync,no_root_squash,no_subtree_check)
# Permisions on the shared Roaming Profile directory -> drwxr-xr-x nobody nogroup moviles/

# Do you want to configure Roaming Profiles? [yes/no]
config_roaming_profiles="no"

# In which Directory/Path will the user have his "home" folder? e.g. "/home/" or "/home/roaming_profiles" or "/roaming_profiles"
roaming_profiles_sys_path="/home/movil"

# NFS server IP address e.g. "192.168.1.100" or Probably the same as LDAP server
nfs_ip="192.168.10.100"

# NFS share path
nfs_roaming_profiles_sys_path="/moviles"


# Change this to "yes" when the VARIABLES ADOVE are properly configured
configured="no"



#### THE SCRIPT STARTS HERE, DON'T MODOFY NOTHING BELOW THIS LINE ####

# Get domain splited: dc=ldap,dc=domain,dc=com
domains_splitted=($(echo $ldap_domain | awk -F. '{ for ( i=1; i<=NF; i++ ) print $i}'))
for i in "${domains_splitted[@]}"; do
	dc+="dc=$i,"
done
domain_dc=$(head -c -2 <<< $dc; echo)

# Get Display Manager
DM=$(awk -F'/' '{ print $NF }' /etc/X11/default-display-manager)


# Check if the script is running with Admin privileges
if [[ $(id -u) -ne 0 ]]; then
	error_msg "MUST BE EXECUTED AS ROOT :)"
fi

if [[ $configured = "yes" ]]; then
	echo -e "\nYour LDAP domain is: ${UNDERLINE}$ldap_domain${NORMALCOLOR}" \
			"\nYour LDAP IP is: ${UNDERLINE}$ldap_ip${NORMALCOLOR}"

	if [[ $config_roaming_profiles = "yes" ]]; then
		echo -e "\nConfig roaming profiles: ${UNDERLINE}$config_roaming_profiles${NORMALCOLOR}" \
				"\nYour LDAP IP is: ${UNDERLINE}$nfs_ip${NORMALCOLOR}" \
				"\nYour LDAP roaming profiles will create in this system path: ${UNDERLINE}$roaming_profiles_sys_path${NORMALCOLOR}" \
				"\nYour LDAP NFS server share is in this path: ${UNDERLINE}$nfs_roaming_profiles_sys_path${NORMALCOLOR}"
			
	fi
else
	error_msg "To run this script you must have configure it, open it with any text editor enter your values to the variables"
fi

# Installing packages non-interactivelly
info_msg "Installing necesary packages to join the LDAP Server..."
export DEBIAN_FRONTEND=noninteractive
apt update && apt install -y libnss-ldap libpam-ldap
dpkg-query -W -f='${Package}\t\t${Version}\n' {libnss-ldap,libpam-ldap} || error_msg "There were problems installing the necessary packages"

# Config /etc/ldap.conf
info_msg "Configuring LDAP (/etc/ldap.conf)..."
sed -i -e "s|^\(base \).*|\1$domain_dc|" /etc/ldap.conf
sed -i -e "s|^uri.*|uri ldap://$ldap_ip|" /etc/ldap.conf
sed -i -e "s|^\(rootbinddn .*\)|#\1|" /etc/ldap.conf

# Config /etc/nsswitch ALSO REMOVE SYSTEMD ENTRY, IF NOT IS TOO SLOW
info_msg "Configuring NSS (/etc/nsswitch.conf)..."
sed -i -e "s/^\(passwd:.*\)/\1 ldap/" /etc/nsswitch.conf
sed -i -e "s/^\(group:.*\)/\1 ldap/" /etc/nsswitch.conf
sed -i -e "s/^\(shadow:.*\)/\1 ldap/" /etc/nsswitch.conf
# gshadow nothing 

# Config /etc/pam.d/common-session
info_msg "Configuring PAM (/etc/pam.d/common-session & /etc/pam.d/common-password)..."
echo "session required pam_mkhomedir.so skel=/etc/skel/ umask=0022 silent" >> /etc/pam.d/common-session

# Config /etc/pam.d/common-password
sed -i -e "s/use_authtok//" /etc/pam.d/common-password

# /etc/ldap/ldap.conf NO ES NECESARIO

#getent passwd
info_msg "These're the LDAP users from ${UNDERLINE}$ldap_ip${NORMALCOLOR} on ${UNDERLINE}$domain_dc${NORMALCOLOR}"

# Config manual login on the default Display Manager
case $DM in 
	lightdm)
		info_msg "Configuring $DM (/etc/lightdm/lightdm.conf.d/manual-login.conf)"
		echo -e "[SeatDefaults]\ngreeter-show-manual-login=true" >> /etc/lightdm/lightdm.conf.d/manual-login.conf
		;;
	sddm|lxdm|gdm*)
		info_msg "By default you have manual login configured on your Display Manager: $DM"
		;;
	*)
		info_msg "This script doesn't support your Display Manager (e.g. LightDM, GDM, SDDM, LXDM...), but you probably don't have to worry"
esac

# Roaming profiles
# IMPORTANT: KNOW THE SERVER SHARED FOLDER
# AND WHAT HAPPENS IF THE USER HASN'T LOGGED IN YET
if [[ $config_roaming_profiles = "yes" ]]; then
    info_msg "Installing necesary packages for Roaming Profiles..."
    apt install -y nfs-common
    
    info_msg "Creating and configuring Roaming profiles Directory..."
    mkdir -p $roaming_profiles_sys_path
    chown nobody:nogroup $roaming_profiles_sys_path
    
    
    info_msg "Configuring Roaming profiles Share to mount automatically at startup..."
    echo "$nfs_ip:$nfs_roaming_profiles_sys_path $roaming_profiles_sys_path nfs auto,noatime,nolock,bg,nfsvers=3,intr,tcp,actimeo=1800 0 0" >> /etc/fstab
    mount -a || error_msg "There were problems mounting the Roaming Profiles NFS shares"
fi

echo -e "\n${SUCCESSCOLOR}Reboot your system for changes to effect${NORMALCOLOR}\n"

